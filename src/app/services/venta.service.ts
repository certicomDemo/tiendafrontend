import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VentaService {
  
  private url="http://localhost:8080/ventas"

  constructor(private http:HttpClient) {

   }

  getVentasPorFecha(fecha:any){ 
    return this.http.get(`${this.url}/fecha/${fecha}`);
  }
  getVentas(){ 
    return this.http.get(`${this.url}/`);
  }
  getVentasById(id:number){ 
    return this.http.get(`${this.url}/${id}`);
  }
}
