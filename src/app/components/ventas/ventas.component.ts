import { Component, OnInit } from '@angular/core';
import { VentaService } from 'src/app/services/venta.service';
import * as moment from 'moment';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.scss']
})
export class VentasComponent implements OnInit {
fecha='';
ventas :any={};
detalles:Array<any>=[];

detalleVenta:any;

  constructor(private ventaService:VentaService) { }

  ngOnInit(): void {
    this.fecha=this.getToday();

    this.buscarPorFecha();
/* this.buscarVentas(); */
  }

 buscarPorFecha() {
  this.ventaService.getVentasPorFecha(moment(this.fecha).format('yyyy-MM-DD')).subscribe((rest: any) => {
    this.ventas = rest;
    console.log(this.ventas);
  })
 }
 buscarVentas() {
  this.ventaService.getVentas().subscribe((rest: any) => {
    this.ventas = rest;
    console.log(this.ventas);
  })
 }
 verDetalle(id:number){
   this.ventaService.getVentasById(id).subscribe((rest:any)=>{
     this.detalleVenta=rest;
     this.detalles=this.detalleVenta.ventaDetalle
     console.log(rest);
   })

 }

 getToday(){
   return moment().format('yyyy-MM-DD');
 }

}
